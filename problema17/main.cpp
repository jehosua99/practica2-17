#include <iostream>

using namespace std;
/*
El programa recibe un número y halla la suma de todos los números amigables menores al número ingresado.
*/
int numeroAmigo(int, int *);
int main()
{
    int numeroIngresado, divisores = 0;
    cout << "Ingrese el numero con el cual se deberan buscar los numeros amigables menores:\n";
    cin >> numeroIngresado;
    int sumaDivisoresIntentados[numeroIngresado];  //arreglo del tamaño del numero ingresado
    for (int i = 0; i <= numeroIngresado; i ++)  //i es el numero al que le busco los divisores
    {
        for (int posiblesDiv = 1; posiblesDiv < i; posiblesDiv++)  //La variable se llama posibles divisores ya que recorre todos los numeros desde 0 hasta i buscando aquellos que lo dividen enteramente
        {
            if (i % posiblesDiv == 0)
                divisores += posiblesDiv;  //divisores es la variable donde se suman los numeros que dividen exactamente al numero i
        }
        sumaDivisoresIntentados[i] = {divisores};  //agrego en la posicion del numero la suma de sus divisores
        divisores = 0;  //reinicio la variable divisores para calcular los del siguiente numero
    }
    cout << numeroAmigo(numeroIngresado, sumaDivisoresIntentados) << " es la suma de los numeros amigos menores que " << numeroIngresado << endl;
    return 0;
}

int numeroAmigo(int ingresado, int divisores[])  //Esta funcion encontrara numeros amigos y los sumara
{
    int  amigos = 0; //varable que contendra la suma de los numeros amigos
    for (int i = (ingresado-1); i != 0; i--)   //Para recorrer por completo la lista debemos empezar desde el final
    {
        for (int j = 0; j < i; j ++)  //Esta variable recorre la lista desde el principio
        {
            if (divisores[i] == j && divisores[j] == i)  //Si la suma de los divisores de los numeros son iguales al otro numero esos numeros son amigos
            {
                amigos += j; //La suma de los divisores de j es i y visceversa por tanto sumamos ambos y encontramos la suma de los numeros amigos
                amigos += i;
            }
        }
    }
    return amigos;
}
